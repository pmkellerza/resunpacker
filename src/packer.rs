use crate::internals::*;

///Type Aliases
pub type ArchiveName = String;
//pub type AssetPath = Arc<str>;
pub type AssetPath = String;
pub type PackerAssets = HashMap<String, PackerAsset>;

#[derive(Debug, Deserialize, Serialize)]
pub struct PackerConfig {
    pub compress: bool,
    pub encrypt: bool,
    pub out_dir: String,
}

impl PackerConfig {
    pub fn default() -> PackerConfig {
        PackerConfig {
            compress: true,
            encrypt: false,
            out_dir: "./out".to_string(),
        }
    }
}

#[derive(Debug, Deserialize, Serialize)]
pub struct PackerAsset {
    path: AssetPath,
    archive_name: ArchiveName,
    filenames: Vec<String>,
}
