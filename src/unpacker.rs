use crate::internals::*;
use std::fs;

#[derive(Debug)]
pub struct ResUnpacker {
    config: PackerConfig,
}

impl ResUnpacker {
    pub fn new(config_file: String) -> Self {
        let config: PackerConfig =
            ron::from_str(&fs::read_to_string(config_file).unwrap()).unwrap();

        Self { config }
    }

    pub fn get_asset(
        &self,
        category: impl Into<String>,
        asset_filename: impl Into<String>,
    ) -> Option<Vec<u8>> {
        let category = category.into();
        let asset_filename = asset_filename.into();

        let tar_file = std::fs::File::open(format!("./assets/{}.pak", category)).unwrap();
        let mut file = Archive::new(tar_file);

        //asset buffer
        let mut asset: Vec<u8> = Vec::new();

        for entry in file.entries().unwrap() {
            let mut file = entry.unwrap();
            let path = file.path().unwrap().to_str().unwrap().to_string();

            println!("Path: {path}");

            //read file from archive to buffer
            if path == asset_filename {
                file.read_to_end(&mut asset);

                if self.config.encrypt {
                    asset = deencrypt_data(String::from_utf8(asset).unwrap());
                }

                if self.config.compress {
                    asset = decompress_data(asset);
                }
            }

            return Some(asset);
        }

        None
    }

    pub fn get_assets<F: FnMut((String, Vec<u8>))>(
        &self,
        category: impl Into<String>,
        mut each: F,
    ) {
        let category = category.into();

        let tar_file = std::fs::File::open(format!("./assets/{}.pak", category)).unwrap();
        let mut file = Archive::new(tar_file);

        for entry in file.entries().unwrap() {
            let mut asset: Vec<u8> = Vec::new();

            let mut file = entry.unwrap();

            file.read_to_end(&mut asset);

            if self.config.encrypt {
                asset = deencrypt_data(String::from_utf8(asset).unwrap());
            }

            if self.config.compress {
                asset = decompress_data(asset);
            }

            let filename = file.path().unwrap();
            let filename = filename.file_name().unwrap().to_str().unwrap();

            let id: Vec<&str> = filename.split(".").collect();

            each((id[0].to_string(), asset.clone()))
        }
    }
}

fn deencrypt_data(data: String) -> Vec<u8> {
    let mc = new_magic_crypt!("VEXARGAMEENGINE", 64);
    mc.decrypt_base64_to_bytes(data).unwrap()
}

fn decompress_data(data: Vec<u8>) -> Vec<u8> {
    let mut decoder = flate2::read::ZlibDecoder::new(&data[..]);
    let mut decompress_data = Vec::new();

    let _res = decoder.read_to_end(&mut decompress_data);

    decompress_data
}

pub fn save_asset_to_file(asset: Vec<u8>, path: impl Into<String>) {
    let path = path.into();

    //check if out_path directory exist create if not
    if !Path::new("./out").exists() {
        std::fs::DirBuilder::new().create("./out").unwrap();
    }

    let mut file = fs::OpenOptions::new()
        .create(true)
        .write(true)
        .open(path)
        .unwrap();

    file.write_all(&asset[..]).unwrap();
}
