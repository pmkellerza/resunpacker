#![allow(unused_imports)]

//Core
pub use std::collections::HashMap;
pub use std::io::prelude::*;
pub use std::io::BufWriter;
pub use std::path::Path;
pub use std::sync::Arc;

//filesystem ulitiy
pub use walkdir::WalkDir;

//Compression
pub use flate2::write::ZlibEncoder;
pub use flate2::Compression;

//encryption
pub use magic_crypt::{new_magic_crypt, MagicCryptTrait};

//archiving
extern crate tar;
pub use tar::{Archive, Builder, Header};

//Serialization
pub use ron::ser::PrettyConfig;
pub use serde::{Deserialize, Serialize};

//Internal
pub use crate::packer::*;
